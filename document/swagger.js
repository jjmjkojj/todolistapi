var spec = {
  "swagger": "2.0",
  "info": {
    "description": "Webservice API",
    "version": "1.0.0",
    "title": "Swagger Petstore"
  },
  "host": "localhost:7777",
  "basePath": "/",
  "tags": [
    {
      "name": "task",
      "description": "todo list"
    }
  ],
  "paths": {
    "/task": {
      "get": {
        "tags": [
          "task"
        ],
        "summary": "Get all todo list",
        "responses": {
          "200": {
            "description": "all todo list",
            "schema": {
              "$ref": "#/definitions/Task"
            }
          }
        }
      },
      "post": {
        "tags": [
          "task"
        ],
        "summary": "Add a new todo list",
        "description": "",
        "operationId": "addPet",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "Add a new todo list",
            "required": true,
            "schema": {
              "$ref": "#/definitions/addTask"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "add todo list success",
            "schema": {
              "$ref": "#/definitions/Task"
            }
          }
        }
      }
    },
    "/task/{id}": {
      "put": {
        "tags": [
          "task"
        ],
        "summary": "Update an existing todo list",
        "description": "",
        "operationId": "updatePet",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "required": true,
            "description": "todo list id",
            "type": "integer",
            "format": "int64"
          },
          {
            "in": "body",
            "name": "body",
            "description": "todo list object that needs to be update",
            "required": true,
            "schema": {
              "$ref": "#/definitions/addTask"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "update success"
          }
        }
      },
      "delete": {
        "tags": [
          "task"
        ],
        "summary": "delete an existing todo list",
        "description": "",
        "operationId": "",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "required": true,
            "description": "todo list id",
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "200": {
            "description": "delete success"
          }
        }
      },
      "patch": {
        "tags": [
          "task"
        ],
        "summary": "Update status to success",
        "description": "",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "required": true,
            "description": "todo list id",
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "200": {
            "description": "update success"
          }
        }
      }
    }
  },
  "definitions": {
    "Task": {
      "type": "object",
      "required": [
        "subject"
      ],
      "properties": {
        "id": {
          "type": "integer",
          "example": 1
        },
        "subject": {
          "type": "string",
          "example": "วิ่ง"
        },
        "detail": {
          "type": "string",
          "example": "รายละเอียด"
        },
        "status": {
          "type": "integer",
          "example": 0
        }
      }
    },
    "addTask": {
      "type": "object",
      "required": [
        "subject"
      ],
      "properties": {
        "subject": {
          "type": "string",
          "example": "วิ่ง"
        },
        "detail": {
          "type": "string",
          "example": "รายละเอียด"
        }
      }
    },
    "ApiResponse": {
      "type": "object",
      "properties": {
        "success": {
          "type": "boolean"
        },
        "data": {
          "type": "object"
        },
        "message": {
          "type": "string"
        }
      }
    }
  },
  "externalDocs": {
    "description": "Find out more about Swagger",
    "url": "http://swagger.io"
  }
}