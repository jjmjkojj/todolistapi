var mysql = require('../../server/db.js');
var db = mysql.Open();
var todo = {};
module.exports = todo;

todo.List = function (callback) {
    return db.query('SELECT * FROM todo', callback);
}
todo.Get = function (id, callback) {
    return db.query('SELECT * FROM todo WHERE id = ?', [id], callback);
}
todo.Add = function (todo, callback) {
    return db.query("INSERT INTO todo (subject, detail) values(?,?)", [todo.subject, todo.detail], callback);
};
todo.Update = function (id, todo, callback) {
    return db.query("UPDATE todo SET subject=?, detail=? WHERE id=?", [todo.subject, todo.detail, id], callback);
};
todo.Delete = function (id, callback) {
    return db.query("DELETE FROM todo WHERE id=?", [id], callback);
};
todo.Success = function (id, callback) {
    return db.query("UPDATE todo SET status=1 WHERE Id=?", [id], callback);
};