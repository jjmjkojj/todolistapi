'use strict';
var todoList = {
    path: 'task',
    pathId: 'task/:id'
};
module.exports = todoList;

var model = require('../../model/todoList');

// register service todo list
todoList.RegisterService = function (server) {

    server.RegisterService(server.GET, todoList.path, todoList.handleGet);
    server.RegisterService(server.POST, todoList.path, todoList.handlePost);

    server.RegisterService(server.GET, todoList.pathId, todoList.handleGet);
    server.RegisterService(server.PUT, todoList.pathId, todoList.handlePut);
    server.RegisterService(server.DELETE, todoList.pathId, todoList.handleDelete);
    server.RegisterService(server.PATCH, todoList.pathId, todoList.handlePatch);

};

todoList.handleGet = function (ctx) {
    var id = ctx.req.params['id'];
    if (id) {
        model.Get(id, function (err, rows) {
            if (err) {
                ctx.error(err);
            } else {
                if (rows instanceof Array) {
                    ctx.json(rows[0]);
                } else {
                    ctx.json(rows);
                }

            }
        });
    } else {
        model.List(function (err, rows) {
            if (err) {
                ctx.error(err);
            } else {
                ctx.json(rows);
            }
        });
    }
};

todoList.handlePost = function (ctx) {
    var json = ctx.req.body;
    model.Add(json, function (err, rs) {
        if (err) {
            ctx.error(err);
        } else {
            json.id = rs.insertId;
            json.status = 0;
            ctx.json(json);
        }
    });
};
todoList.handlePut = function (ctx) {
    var json = ctx.req.body;
    var id = ctx.req.params['id'];
    if (id) {
        model.Update(id, json, function (err, rs) {
            if (err) {
                ctx.error(err);
            } else {
                if (rs.affectedRows) {
                    ctx.json('update success');
                } else {
                    ctx.error('no task', id);
                }
            }
        });
    }
};
todoList.handleDelete = function (ctx) {
    var id = ctx.req.params['id'];
    if (id) {
        model.Delete(id, function (err, rs) {
            if (err) {
                ctx.error(err);
            } else {
                if (rs.affectedRows) {
                    ctx.json('update success');
                } else {
                    ctx.error('no task', id);
                }
            }
        });
    }
};
todoList.handlePatch = function (ctx) {
    var id = ctx.req.params['id'];
    if (id) {
        model.Success(id, function (err, rs) {
            if (err) {
                ctx.error(err);
            } else {
                if (rs.affectedRows){
                    ctx.json('update success');
                }else{
                    ctx.error('no task', id);
                }
            }
        });
    }
};