## สิ่งที่ต้องใช้

1. ภาษาหลักที่ใช้ทำ service **Node js** [download](https://nodejs.org/en/)
2. database **MySQL** [download](https://www.apachefriends.org/download.html)

---

## สิ่งที่ต้องทำก่อนเปิด server

1. แก้ไข config สำหรับ connect **MySQL** ที่ server/db.js
2. สร้าง table todo (server/todo.sql)

---

## วิธีเปิด server

1. เปิด MySQL 
2. ใช้คำสั่ง npm start ในโฟลเดอร์ todoListApi เพื่อเปิด service
3. api-docs จะอยู่ที่ [http://localhost:7777/](http://localhost:7777/)

---