var server = require('./server/server.js');

var todoList = require('./api/service/todoList/main.js');


server.RegisterService(server.GET, 'api-docs', function (ctx) {
    var config = require('./swagger.json');
    console.log(config);
    ctx.res.json(config);
});

todoList.RegisterService(server);
server.Start();