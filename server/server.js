'use strict';
var server = {
    GET: 'GET',
    POST: 'POST',
    PUT: 'PUT',
    DELETE: 'DELETE',
    PATCH: 'PATCH',
    count: 0
};
// start api server
server.Start = function () {
    server.express = require('express');
    server.app = server.express();
    server.port = 7777;

    require('./db.js').Init();

    var bodyParser = require('body-parser');
    server.app.use(bodyParser.urlencoded({
        extended: true
    }));
    server.app.use(bodyParser.json());

    server.loadSwagger();
    server.loadService();

    server.app.listen(server.port);
    console.log('RESTful API server started on: ' + server.port);
    console.log('============================');
};

// register sevice
// @param {String} method - http method (get, post, put, delete, patch)
// @param {String} path - url path
// @param {Function} callback - callback function
server.RegisterService = function (method, path, callback) {
    if (typeof path !== 'string' || typeof method !== 'string' || typeof callback !== 'function') {
        return false;
    }
    path = path.trim();
    if (path.charAt(0) != '/') {
        path = '/' + path;
    }
    method = method.toUpperCase();
    if (!server.service) {
        server.service = {};
    }
    switch (method) {
        case server.GET:
        case server.POST:
        case server.PUT:
        case server.DELETE:
        case server.PATCH:
            if (!server.service[method]) {
                server.service[method] = {};
            }
            var func = function (req, res) {
                var logId = server.LogService(req, res);
                var rp = server.Context(req, res, logId);
                callback(rp);
            }
            server.service[method][path] = func;
            break;
        default:
            console.log('register service', path, 'fail undefiend method {', method, '}');
    }
    console.log('register sevice', method, path, 'successful');
};

server.loadSwagger = function () {
    server.app.use(server.express.static('document'));
    server.app.get('/', function (req, res, next) {
        var fs = require('fs')
        fs.readFile(__dirname + '/../document/index.html', 'utf8',
            function (err, data) {
                if (err) {
                    return console.log(err);
                }
                res.set('Content-Type', 'text/html; charset=utf-8');
                res.send(data);
            });
    });
}

server.loadService = function () {
    for (var mt in server.service) {
        var s = server.service[mt];
        for (var k in s) {
            var cb = s[k];
            switch (mt) {
                case server.GET:
                    server.app.get(k, cb);
                    break;
                case server.POST:
                    server.app.post(k, cb);
                    break;
                case server.PUT:
                    server.app.put(k, cb);
                    break;
                case server.DELETE:
                    server.app.delete(k, cb);
                    break;
                case server.PATCH:
                    server.app.patch(k, cb);
                    break;
            }
        }
    }
};
server.LogService = function (req, res) {
    server.count++;
    console.log(server.count + ' #>> ' + req.ip + ' ' + req.method + ' ' + req.originalUrl);
    return server.count;
};
server.LogReply = function (logId, status, msg) {
    console.log(logId + ' <<# [' + status + '] ' + msg);
}



server.Context = function (req, res, logId) {
    return {
        req: req,
        res: res,
        json: function (js) {
            server.LogReply(logId, 200, '');
            res.json({
                success: true,
                data: js
            });
        },
        error: function (msg) {
            res.statusCode = 500;
            res.json({
                success: false,
                message: msg
            });
            server.LogReply(logId, 500, msg);
        }
    };
}

module.exports = server;