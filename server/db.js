var mysql = require('mysql')
var db = {
    conn: null,
    Host: 'localhost',
    User: 'root',
    Password: '',
    Database: 'test',
};
module.exports = db;

db.Init = function(){
    db.conn = mysql.createConnection({
        host: db.Host,
        user: db.User,
        password: db.Password,
        database: db.Database
    });
    db.conn.connect();
}

db.Open = function(){
    if ( !db.conn){
        db.Init();
    }
    return db.conn;
}
